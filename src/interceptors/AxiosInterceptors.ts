import { Injectable } from '@angular/core';
import { AxiosResponse, InternalAxiosRequestConfig } from 'axios';
import General from 'src/models/General';

@Injectable({ providedIn: 'root' })
export default class AxiosInterceptor {
  constructor(public model: General) {}

  public reqConfig = (config: InternalAxiosRequestConfig<any>) => {
    this.model.buscandoWS = true;

    return config;
  };

  public reqError = (error: any) => {
    this.model.buscandoWS = false;
    return Promise.reject(error);
  };

  public resOk = (response: AxiosResponse<any, any>) => {
    this.model.buscandoWS = false;

    return response;
  };

  public resError = (error: any) => {
    this.model.buscandoWS = false;
    return Promise.reject(error);
  };
}
