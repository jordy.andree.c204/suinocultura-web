export interface Database {
  granjas: Granja[];
}

export interface Granja {
  idGranja: string;
  nome: string;
  capacidade: number;
  galpao: Galpao[];
}

export interface Galpao {
  idGalpao: string;
  nomeGalpao: string;
  capacidade: number;
  qtdBaias: number;
  baias: Baia[];
}

export interface Baia {
  idBaia: string;
  nomeBaia: string;
  dataAlojamento: string;
  qtdSuinos: number;
  eventos?: Evento[];
  //idSuinos: number[];
}

export interface Evento {
  idEvento?: string;
  tipo: string;
  finalidade: string;
  quantidade: number;
  data: string;
  idSuinos?: number[];
  corEvento: string;
}
