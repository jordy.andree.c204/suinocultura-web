import { Injectable } from '@angular/core';
import { Galpao } from './Database';

@Injectable({ providedIn: 'root' })
export default class General {
  public overlay: boolean = false;
  public buscandoWS: boolean = false;

  public capa_total: number = 0;
  public galpoes: Galpao[] = [];
}
