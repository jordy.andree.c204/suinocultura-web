import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { Galpao } from 'src/models/Database';

const url = 'https://e0c0-187-49-86-106.ngrok-free.app';

export const ws_beans_header: AxiosRequestConfig<string> = {
  maxBodyLength: Infinity,
  headers: {
    'ngrok-skip-browser-warning': '69420',
    'Content-Type': 'application/json',
  },
};

export const get = async () =>
  (
    await axios.get<AxiosResponse<Galpao[]>, AxiosResponse<Galpao[]>>(
      `${url}/granjas`,
      ws_beans_header
    )
  ).data;

export const post = async (param: string, body: string = '{}') =>
  (
    await axios.post<AxiosResponse<any>, AxiosResponse<any>>(
      `${url}/${param}`,
      body,
      ws_beans_header
    )
  ).data;
