import { Injectable } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Injectable({ providedIn: 'root' })
export default class FormDesign {
  constructor() {}

  public tabs: MenuItem[] = [
    {
      label: 'Inicio',
      routerLink: '/',
      styleClass: 'text-3xl',
    },
    {
      label: 'Galpões',
      routerLink: 'galpoes',
      styleClass: 'text-3xl',
    },
  ];
}
