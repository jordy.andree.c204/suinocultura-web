import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-div-input[label]',
  templateUrl: './div-input.component.html',
  styleUrls: ['./div-input.component.scss']
})
export class DivInputComponent {
  @Input() label: string = '';
}
