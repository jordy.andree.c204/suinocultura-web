import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DivInputComponent } from './div-input.component';

describe('DivInputComponent', () => {
  let component: DivInputComponent;
  let fixture: ComponentFixture<DivInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DivInputComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DivInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
