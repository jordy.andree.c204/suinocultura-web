import { Component, EventEmitter, Input, Output } from '@angular/core';
import General from 'src/models/General';

type Action = 'close'|'save'

export interface IEvent {
  type: Action;
  toggle_visible: boolean;
  data: any;
}

@Component({
  selector: 'app-modal-cadastro',
  templateUrl: './modal-cadastro.component.html',
  styleUrls: ['./modal-cadastro.component.scss'],
})
export class ModalCadastroComponent {
  @Input() title!: string;
  @Input() width: number = 8;
  @Input() toggle_visible: boolean = false;

  @Output() interact = new EventEmitter<Action>();

  constructor(public model: General) {}

  public emit() {
    this.toggle_visible = false;
    this.interact.emit('close');
  }
}
