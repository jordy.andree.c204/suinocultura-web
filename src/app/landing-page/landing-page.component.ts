import { Component } from '@angular/core';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
})
export class LandingPageComponent {
  public data: any;
  public data2: any;
  public options: any;
  public options2: any;
  public products: any;

  public ngOnInit() {
    const documentStyle = getComputedStyle(document.documentElement);
    const textColor = documentStyle.getPropertyValue('--text-color');

    this.data = {
      labels: ['Azul', 'Vermelho', 'Geral'],
      datasets: [
        {
          data: [300, 50, 100],
          backgroundColor: [
            documentStyle.getPropertyValue('--blue-500'),
            documentStyle.getPropertyValue('--red-500'),
            documentStyle.getPropertyValue('--green-500'),
          ],
          hoverBackgroundColor: [
            documentStyle.getPropertyValue('--blue-400'),
            documentStyle.getPropertyValue('--red-400'),
            documentStyle.getPropertyValue('--green-400'),
          ],
        },
      ],
    };

    this.options = {
      cutout: '60%',
      plugins: {
        legend: {
          labels: {
            color: textColor,
            fontSize: 25,
          },
        },
      },
    };

    this.products = [
      {
        code: '1.3.1',
        name: 'Baia 1',
        quantity: 24,
        data: '02/01/2024',
      },
      {
        code: '1.3.4',
        name: 'Baia 4',
        quantity: 21,
        data: '02/01/2024',
      },
      {
        code: '1.5.1',
        name: 'Baia 1',
        quantity: 16,
        data: '02/01/2024',
      },
      {
        code: '1.5.3',
        name: 'Baia 3',
        quantity: 10,
        data: '02/01/2024',
      },
      {
        code: '1.5.4',
        name: 'Baia 4',
        quantity: 4,
        data: '02/01/2024',
      },
    ];
  }
}
