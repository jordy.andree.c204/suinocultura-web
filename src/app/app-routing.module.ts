import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { GalpoesComponent } from './galpoes/galpoes.component';
import { BaiasComponent } from './baias/baias.component';
import { BaiaComponent } from './baias/baia/baia.component';

const routes: Routes = [
  {
    path: '',
    component: LandingPageComponent,
  },
  {
    path: 'galpoes',
    component: GalpoesComponent,
    children: [
    ],
  },
  {
    path: 'galpoes/:galpaoId/baias',
    component: BaiasComponent,
  },
  {
    path: 'galpoes/:galpaoId/baias/:baiaId',
    component: BaiaComponent,
  },
  { path: '**', redirectTo: '/' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
