import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Table } from 'primeng/table';
import { Baia } from 'src/models/Database';
import General from 'src/models/General';
import { post } from 'src/functions/WebService';

type Action = 'close' | 'save';

@Component({
  selector: 'app-baias',
  templateUrl: './baias.component.html',
  styleUrls: ['./baias.component.scss'],
})
export class BaiasComponent {
  public galpaoId: string = '';
  public baias: Baia[] = [];
  public baia?: Baia;
  public columns = [
    { label: 'Código', key: 'idBaia', class: 'col-2' },
    { label: 'Nome', key: 'nomeBaia', class: 'col-3' },
    { label: 'Quantidade de Suinos', key: 'qtdSuinos', class: 'col-3' },
    { label: 'Data alojamento', key: 'dataAlojamento', class: 'col-3' },
  ];
  public filters: string[] = this.columns.map((c) => c.key);

  public tmodal: boolean = false;
  public nomeBaia: string = '';
  public qtdSuinos: number = 0;
  public dataAlojamento: string = '';

  constructor(public model: General, private activatedRoute: ActivatedRoute) {
    this.galpaoId = this.activatedRoute.snapshot.params['galpaoId'];
  }

  public async ngOnInit(): Promise<void> {
    this.baias = await post(
      'baias',
      JSON.stringify({ idGalpao: this.galpaoId })
    );
  }

  public searchBaia(b: Baia) {
    this.baia = b;
    return `/galpoes/${this.galpaoId}/baias/${this.baia?.idBaia}`;
  }

  public inputFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as any).value, 'contains');
  }

  public open(e?: Baia) {
    if (e) {
      this.nomeBaia = e.nomeBaia;
      this.qtdSuinos = e.qtdSuinos;
      var [DD, MM, YYYY] = e.dataAlojamento.split('/');
      if (DD.length == 1) DD = '0' + DD;
      if (MM.length == 1) MM = '0' + MM;
      this.dataAlojamento = `${YYYY}-${MM}-${DD}`;
    }
    this.tmodal = true;
  }

  public interact(a: Action) {
    if (a == 'save') {
      var [YYYY, MM, DD] = this.dataAlojamento.split('-');
      if (DD.length == 1) DD = '0' + DD;
      if (MM.length == 1) MM = '0' + MM;
      this.dataAlojamento = `${DD}/${MM}/${YYYY}`;
    }
    this.nomeBaia = '';
    this.qtdSuinos = 0;
    this.dataAlojamento = '';
    this.tmodal = false;
  }
}
