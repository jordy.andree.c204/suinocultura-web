import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BaiasComponent } from './baias.component';

describe('BaiasComponent', () => {
  let component: BaiasComponent;
  let fixture: ComponentFixture<BaiasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BaiasComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BaiasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
