import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BaiaComponent } from './baia.component';

describe('BaiaComponent', () => {
  let component: BaiaComponent;
  let fixture: ComponentFixture<BaiaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BaiaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BaiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
