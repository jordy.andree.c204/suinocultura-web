import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SelectItem } from 'primeng/api';
import { Table } from 'primeng/table';
import { post } from 'src/functions/WebService';
import { Baia, Evento } from 'src/models/Database';
import General from 'src/models/General';

type Action = 'close' | 'delete' | 'save';

@Component({
  selector: 'app-baia',
  templateUrl: './baia.component.html',
  styleUrls: ['./baia.component.scss'],
})
export class BaiaComponent {
  @Input() toggle_visible: boolean = false;

  public galpaoId: string = '';
  public baiaId: string = '';
  public baia: Baia;
  public columns = [
    { label: 'Tipo', key: 'tipo', class: 'col-3' },
    { label: 'Finalidade', key: 'finalidade', class: 'col-2' },
    { label: 'Data', key: 'data', class: 'col-2' },
  ];
  public filters: string[] = this.columns.map((c) => c.key);

  public options: SelectItem[] = [
    { label: 'Geral', value: 'Geral' },
    { label: 'Azul', value: 'Azul' },
    { label: 'Vermelho', value: 'Vermelho' },
  ];

  public tmodal: boolean = false;
  public tipo: string = '';
  public finalidade: string = '';
  public quantidade: number = 0;
  public data: string = '';
  public corEvento: string = '';

  constructor(public model: General, private activatedRoute: ActivatedRoute) {
    this.galpaoId = this.activatedRoute.snapshot.params['galpaoId'];
    this.baiaId = this.activatedRoute.snapshot.params['baiaId'];
    this.baia = model.galpoes
      .find((g) => g.idGalpao == this.galpaoId)
      ?.baias.find((g) => g.idBaia == this.baiaId)!;
  }

  public async ngOnInit(): Promise<void> {
    this.baia.eventos = await post(
      'eventos',
      JSON.stringify({ idGalpao: this.galpaoId, idBaia: this.baiaId })
    );
  }

  public getIcon(s: string) {
    switch (s) {
      case 'Geral':
        return 'pi pi-file';
      case 'Vermelho':
        return 'pi pi-exclamation-triangle';
      case 'Azul':
        return 'pi pi-info-circle';
      default:
        return '';
    }
  }
  public getSeverity(s: string) {
    switch (s) {
      case 'Geral':
        return 'success';
      case 'Vermelho':
        return 'danger';
      case 'Azul':
        return 'info';
      default:
        return '';
    }
  }

  public inputFilter(table: Table, event: Event) {
    table.filterGlobal((event.target as any).value, 'contains');
  }

  public open(e?: Evento) {
    if (e) {
      this.tipo = e.tipo;
      this.finalidade = e.finalidade;
      this.quantidade = e.quantidade;
      var [DD, MM, YYYY] = e.data.split('/');
      if (DD.length == 1) DD = '0' + DD;
      if (MM.length == 1) MM = '0' + MM;
      this.data = `${YYYY}-${MM}-${DD}`;
      this.corEvento = e.corEvento;
    }
    this.tmodal = true;
  }

  public async interact(a: Action) {
    if (a == 'save') {
      var [YYYY, MM, DD] = this.data.split('-');
      if (DD.length == 1) DD = '0' + DD;
      if (MM.length == 1) MM = '0' + MM;
      this.data = `${DD}/${MM}/${YYYY}`;
      await post(
        'addEvento',
        JSON.stringify({
          idGalpao: this.galpaoId,
          idBaia: this.baiaId,
          evento: {
            idEvento: '',
            tipo: this.tipo,
            finalidade: this.finalidade,
            quantidade: this.quantidade,
            data: this.data,
            corEvento: this.corEvento,
          },
        })
      );
    } else if (a == 'delete') {
    }
    this.tipo = '';
    this.finalidade = '';
    this.quantidade = 0;
    this.data = '';
    this.corEvento = '';
    this.tmodal = false;
    if (a != 'close') await this.ngOnInit();
  }
}
