import { Component, Input } from '@angular/core';

type Action = 'close' | 'save';

@Component({
  selector: 'app-galpao',
  templateUrl: './galpao.component.html',
  styleUrls: ['./galpao.component.scss']
})
export class GalpaoComponent {
  @Input() id: string = '';
  @Input() nome: string = '';
  @Input() capacidade: number = 0;
  @Input() qbaias: number = 0;

  public tmodal: boolean = false;
  public nomeGalpao: string = '';
  public newCapacidade: number = 0;
  public qtdBaias: number = 0;

  public open() {
    this.tmodal = true;
  }

  public interact(a: Action) {
    if (a == 'save') {
    }
    this.nomeGalpao = '';
    this.capacidade = 0;
    this.qtdBaias = 0;
    this.tmodal = false;
  }
}
