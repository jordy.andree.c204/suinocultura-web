import { Component } from '@angular/core';
import { get } from 'src/functions/WebService';
import General from 'src/models/General';

type Action = 'close' | 'save';

@Component({
  selector: 'app-galpoes',
  templateUrl: './galpoes.component.html',
  styleUrls: ['./galpoes.component.scss'],
})
export class GalpoesComponent {
  public tmodal: boolean = false;
  public nomeGalpao: string = '';
  public capacidade: number = 0;
  public qtdBaias: number = 0;

  constructor(public model: General) {}

  public async ngOnInit(): Promise<void> {
    this.model.galpoes = await get();
  }

  public open() {
    this.tmodal = true;
  }

  public interact(a: Action) {
    if (a == 'save') {
    }
    this.nomeGalpao = '';
    this.capacidade = 0;
    this.qtdBaias = 0;
    this.tmodal = false;
  }
}
