import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GalpoesComponent } from './galpoes.component';

describe('GalpoesComponent', () => {
  let component: GalpoesComponent;
  let fixture: ComponentFixture<GalpoesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GalpoesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GalpoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
