import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import axios from 'axios';
import { MenuItem, PrimeNGConfig } from 'primeng/api';
import FormDesign from 'src/functions/FormDesign';
import AxiosInterceptor from 'src/interceptors/AxiosInterceptors';
import General from 'src/models/General';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  public title = 'hackathon-srd2024';

  public tabs: MenuItem[] = this.design.tabs;

  constructor(
    public design: FormDesign,
    public translate: TranslateService,
    public primeNGConfig: PrimeNGConfig,
    public model: General,
    public ai: AxiosInterceptor
  ) {
    translate.addLangs(['pt']);
    translate.setDefaultLang('pt');
    translate.use('pt');
    this.translate
      .stream('primeng')
      .subscribe((data) => this.primeNGConfig.setTranslation(data));
  }

  public ngOnInit(): void {
    axios.interceptors.request.use(this.ai.reqConfig, this.ai.reqError);
    axios.interceptors.response.use(this.ai.resOk, this.ai.resError);
  }
}
